<?php

use Lib\Conexao;

abstract class BaseDAO
{
    private $conexao;

    public function __construct()
    {
        $this->conexao = Conexao::getConnection();
    }

    protected function select($sql)
    {
        if(!empty($sql))
        {
            return $this->conexao->query($sql);
        }
    }
}