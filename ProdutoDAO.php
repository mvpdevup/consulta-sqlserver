<?php

class ProdutoDAO extends BaseDAO
{

    public  function listar($id = null)
    {
        if($id) {
            $resultado = $this->select(
                "SELECT * FROM produto WHERE id = $id"
            );

            return $resultado->fetch();
        }else{
            $resultado = $this->select(
                'SELECT * FROM produto'
            );
            return $resultado->fetchAll(\PDO::FETCH_CLASS, \Produto::class);
        }

        return false;
    }

}