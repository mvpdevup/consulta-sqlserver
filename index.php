<?php
    define('DB_HOST'        , "servertestefabio.database.windows.net");
    define('DB_USER'        , "fabio");
    define('DB_PASSWORD'    , "Devmedia123");
    define('DB_NAME'        , "dbtestefabio");
    define('DB_DRIVER'      , "sqlsrv");

    require_once "Lib\Conexao.php";
    require_once "Produto.php";
    require_once "BaseDAO.php";
    require_once "ProdutoDAO.php";

    try{
        $produtoDAO = new ProdutoDAO();
    }catch(Exception $e){
        echo $e->getMessage();
    }

?>
<table border=1>
    <tr>
        <td>Nome</td>
        <td>Preço</td>
        <td>Quantidade</td>
        <td>Data Cadastro</td>
    </tr>
    <?php
        foreach($produtoDAO->listar() as $produto) {
    ?>
        <tr>
            <td><?php echo $produto->getNome(); ?></td>
            <td>R$ <?php echo $produto->getPreco(); ?></td>
            <td><?php echo $produto->getQuantidade(); ?></td>
            <td><?php echo $produto->getDataCadastro()->format('d/m/Y'); ?></td>
        </tr>
    <?php
        }
    ?>
</table>